Website designed for people that feed birds during the winter available [here](http://hranilica.ptice.net/).

It helps them to see what bird species they can expect on their bird feeders based on the type of feeder and the type of food provided.

# Technologies
- angular
- angular- animate
- bootstrap

# Feature overview
![overview](https://gitlab.com/igadmile/feederBIOM/raw/b86414deb78f48dcd16a4b9503c5366cf3d22998/overview.jpg)

- Choose food type in the "Izaberite hranu" menu.
- Choose feeder type in the "Izaberite hranilicu" menu.
- Bird specie images and names are displayed based on the selected food and feeder type.

![overview](https://gitlab.com/igadmile/feederBIOM/raw/b86414deb78f48dcd16a4b9503c5366cf3d22998/modal.jpg)

Clicking on the bird image will open the modal dialogue displaying additional data for the specie:
- Latin name of the specie.
- Type of food that specie eats.
- Type of feeders that specie visits.

Clicking on the "speaker" image will play the call that specie makes during the winter.
