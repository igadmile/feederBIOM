#!/bin/sh

echo "Skripta za smanjivanje jpgova v1.1"
echo "https://gitlab.com/mzec/ornitozajeb"
find . -name '*.[Jj][pP]*[gG]' | while read line; do
	echo "smanjujem -> $f"
	convert "$line" -resize 250 -quality 75 "$line"
done
