angular.module('feederBIOM', ['ngAnimate']);

angular.module('feederBIOM').controller('speciesTest', ['$scope', function ($scope) {

    $scope.showSpecie = function (specie, food, feeder) {
        if ((specie.food.indexOf(food) > -1 || specie.food === 'sve') && (specie.feeder.indexOf(feeder) > -1 || specie.feeder === 'sve')) {
            return true;
        } else {
            return false;
        }
    };

    $scope.displayInfo = function (bird) {
        $scope.foodDisplay = bird.food;
        $scope.feederDisplay = bird.feeder;
        $scope.birdHRDisplay = bird.HR;
        $scope.birdLatinDisplay = bird.latin;
        $scope.birdImgDisplay = bird.img;
        $scope.birdCall = 'data/sounds/' + bird.img + '.mp3';
        $scope.soundAuthor = bird.soundAuthor;
    };

    $scope.menu = function (menuType) {
        $scope.foodArray.indexOf(menuType) > -1 ? $scope.food = menuType : $scope.feeder = menuType;
    };

    $scope.playCall = function (state) {
        if (state === 'play') {
            document.getElementById('audio-lola').play();
            $scope.playing = true;
        } else {
            document.getElementById('audio-lola').pause();
            $scope.playing = false;
        }
    };

    $scope.food = 'sjemenke suncokreta';
    $scope.feeder = 'hanilica od plastične boce';

    $scope.foodArray = ['sjemenke suncokreta', 'voće', 'usitnjeni kukuruz', 'zob', 'proso', 'orašasti plodovi', 'loj'];
    $scope.feederArray = ['hanilica od plastične boce', 'platforma sa spremnikom', 'otvorena platforma', 'lojna kugla', 'tlo'];

    $scope.birds = birdData;

    $scope.foodDisplay = "";
    $scope.feederDisplay = "";
    $scope.birdHRDisplay = "";
    $scope.birdLatinDisplay = "";
    $scope.birdImgDisplay = "";
    $scope.birdCall = "";
    $scope.soundAuthor = "";
    $scope.playing = false;

    $('#displayData').on('hidden.bs.modal', function (e) {
        $scope.playing = false;
        document.getElementById('audio-lola').pause();
    });
}]);