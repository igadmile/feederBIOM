var birdData = {
    "aegcaud": {
        "latin": "Aegithalos caudatus",
        "EN": "Long-Tailed Tit",
        "HR": "dugorepa sjenica",
        "food": ["sjemenke suncokreta", "kikiriki", "loj"],
        "img": "aegcaud.jpg",
        "region": ["sve"],
        "feeder": ["sve"],
        "call": "http://www.xeno-canto.org/sounds/uploaded/CIMGCGUWCS/XC339918-stjertmeis-1005_102517.mp3"
    },
    "bombgar": {
        "latin": "Bombycilla garrulus",
        "EN": "Bohemian Waxwing",
        "HR": "svilorepa kugara",
        "food": ["sjemenke suncokreta", "kikiriki", "loj"],
        "img": "bombgar.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "callap": {
        "latin": "Calcarius lapponicus",
        "EN": "Lapland Longspur",
        "HR": "laponska strnadica",
        "food": ["sjemenke suncokreta", "kikiriki", "loj"],
        "img": "callap.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "carcan": {
        "latin": "Carduelis cannabina",
        "EN": "Common Linnet",
        "HR": "juričica",
        "food": ["sjemenke suncokreta"],
        "img": "carcan.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "carcar": {
        "latin": "Carduelis carduelis",
        "EN": "European Goldfinch ",
        "HR": "obični češljugar",
        "food": ["sjemenke suncokreta"],
        "img": "carcar.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "carfla": {
        "latin": "Carduelis flammea",
        "EN": "Common Redpoll",
        "HR": "sjverna juričica",
        "food": ["sjemenke suncokreta"],
        "img": "carfla.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "carspicarspi": {
        "latin": "Carduelis spinus",
        "EN": "Siskin",
        "HR": "žuti čižak",
        "food": ["sjemenke suncokreta"],
        "img": "carspi.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "chlochlo": {
        "latin": "Chloris chloris",
        "EN": "European Greenfinch",
        "HR": "zelendur",
        "food": ["sjemenke suncokreta"],
        "img": "chlochlo.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "coccoc": {
        "latin": "Coccothraustes coccothraustes",
        "EN": "Hawfinch",
        "HR": "batokljun",
        "food": ["sjemenke suncokreta"],
        "img": "coccoc.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "colliv": {
        "latin": "Columba livia",
        "EN": "Rock Dove",
        "HR": "divlji golub",
        "food": ["sjemenke suncokreta", "mljeveni kukuruz", "žitarice", "proso", "kikiriki"],
        "img": "colliv.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "colpal": {
        "latin": "Columba palumbus",
        "EN": "Common Woodpigeon",
        "HR": "golub grivnjaš",
        "food": ["sjemenke suncokreta", "mljeveni kukuruz", "proso", "kikiriki"],
        "img": "colpal.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "ccx": {
        "latin": "Corvus cornix",
        "EN": "Hooded crow",
        "HR": "siva vrana",
        "food": ["sjemenke suncokreta", "mljeveni kukuruz", "kikiriki", "loj"],
        "img": "ccx.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "cyacae": {
        "latin": "Cyanistes caeruleus",
        "EN": "Eurasian blue tit",
        "HR": "plavetna sjenica",
        "food": ["sjemenke suncokreta", "kikiriki", "loj"],
        "img": "cyacae.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "denmaj": {
        "latin": "Dendrocopos major",
        "EN": "Great Spotted Woodpecker",
        "HR": "veliki djetlić",
        "food": ["sjemenke suncokreta", "kikiriki", "loj"],
        "img": "denmaj.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "denmed": {
        "latin": "Dendrocopos medius",
        "EN": "Middle spotted woodpecker",
        "HR": "srednji djetlić",
        "food": ["sjemenke suncokreta", "kikiriki", "loj"],
        "img": "denmed.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "denmin": {
        "latin": "Dendrocopos minor",
        "EN": "Lesser spotted woodpecker",
        "HR": "mali djetlić",
        "food": ["sjemenke suncokreta", "kikiriki", "loj"],
        "img": "denmin.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "densyr": {
        "latin": "Dendrocopos syriacus",
        "EN": "Syrian Woodpecker",
        "HR": "sirijski djetlić",
        "food": ["sjemenke suncokreta", "kikiriki", "loj"],
        "img": "densyr.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "drymar": {
        "latin": "Dryocopus martius",
        "EN": "Black Woodpecker",
        "HR": "crna žuna",
        "food": ["sjemenke suncokreta", "kikiriki", "loj"],
        "img": "drymar.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "emcal": {
        "latin": "Emberiza calandra",
        "EN": "Corn Bunting",
        "HR": "velika strnadica",
        "food": ["sjemenke suncokreta", "mljeveni kukuruz", "proso", "kikiriki"],
        "img": "emcal.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "emcia": {
        "latin": "Emberiza cia",
        "EN": "Rock Bunting",
        "HR": "strnadica cikavica",
        "food": ["sjemenke suncokreta", "mljeveni kukuruz", "proso", "kikiriki"],
        "img": "emcia.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "emcirl": {
        "latin": "Emberiza cirlus",
        "EN": "Cirl Bunting",
        "HR": "crnogrla strnadica",
        "food": ["sjemenke suncokreta", "mljeveni kukuruz", "proso", "kikiriki"],
        "img": "emcirl.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "emcit": {
        "latin": "Emberiza citrinella",
        "EN": "Yellowhammer",
        "HR": "žuta strnadica",
        "food": ["sjemenke suncokreta", "mljeveni kukuruz", "proso", "kikiriki"],
        "img": "emcit.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    },
    "emsch": {
        "latin": "Emberiza schoeniclus",
        "EN": "Reed Bunting",
        "HR": "močvarna strnadica",
        "food": ["sjemenke suncokreta", "mljeveni kukuruz", "proso", "kikiriki"],
        "img": "emsch.jpg",
        "region": ["sve"],
        "feeder": ["sve"]
    }
};