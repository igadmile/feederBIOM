var birdData = {
    aegcaud: {
        "latin": "Aegithalos caudatus",
        "EN": "Long-tailed Tit",
        "HR": "dugorepa sjenica",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "aegcaud",
        "feeder": ["platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Terje Kolaas"
    },
    bombgar: {
        "latin": "Bombycilla garrulus",
        "EN": "Bohemian Waxwing",
        "HR": "kugara",
        "food": ["sjemenke suncokreta", "zob", "orašasti plodovi", "loj"],
        "img": "bombgar",
        "feeder": ["platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Hans Matheve"
    },
    carcan: {
        "latin": "Carduelis cannabina",
        "EN": "Eurasian Linnet",
        "HR": "juričica",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "carcan",
        "feeder": ["tlo", "platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma"],
        "soundAuthor": "Marcel Tenhaeff"
    },
    carcar: {
        "latin": "Carduelis carduelis",
        "EN": "European Goldfinch",
        "HR": "češljugar",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "carcar",
        "feeder": ["tlo, platforma sa spremnikom", "hanilica od plastične boce, otvorena platforma"],
        "soundAuthor": "Tero Linjama"
    },
    carfla: {
        "latin": "Carduelis flammea",
        "EN": "Common Redpoll",
        "HR": "sjeverna juričica",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "carfla",
        "feeder": ["tlo", "platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma"],
        "soundAuthor": "Jarek Matusiak"
    },
    carspi: {
        "latin": "Carduelis spinus",
        "EN": "Eurasian Siskin",
        "HR": "čižak",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "carspi",
        "feeder": ["tlo", "platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma"],
        "soundAuthor": "Jerome Fischer"
    },
    chlochlo: {
        "latin": "Carduelis chloris",
        "EN": "European Greenfinch",
        "HR": "zelendur",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "chlochlo",
        "feeder": ["tlo", "platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma"],
        "soundAuthor": "Stanislas Wroza"
    },
    coccoc: {
        "latin": "Coccothraustes coccothraustes",
        "EN": "Hawfinch",
        "HR": "batokljun",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "coccoc",
        "feeder": ["tlo", "platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma"],
        "soundAuthor": "Agostinho Tomás"
    },
    cyacae: {
        "latin": "Parus caeruleus",
        "EN": "Blue Tit",
        "HR": "plavetna sjenica",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "cyacae",
        "feeder": ["platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Stanislas Wroza"
    },
    denmaj: {
        "latin": "Dendrocopos major",
        "EN": "Great Spotted Woodpecker",
        "HR": "veliki djetlić",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "denmaj",
        "feeder": ["platforma sa spremnikom", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Niels Van Doninck"
    },
    denmed: {
        "latin": "Dendrocopos medius",
        "EN": "Middle Spotted Woodpecker",
        "HR": "crvenoglavi djetlić",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "denmed",
        "feeder": ["platforma sa spremnikom", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Tomek Tumiel"
    },
    denmin: {
        "latin": "Dendrocopos minor",
        "EN": "Lesser Spotted Woodpecker",
        "HR": "mali djetlić",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "denmin",
        "feeder": ["platforma sa spremnikom", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "José Carlos Sires"
    },
    densyr: {
        "latin": "Dendrocopos syriacus",
        "EN": "Syrian Woodpecker",
        "HR": "sirijski djetlić",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "densyr",
        "feeder": ["platforma sa spremnikom", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Tomek Tumiel"
    },
    drymar: {
        "latin": "Dryocopus martius",
        "EN": "Black Woodpecker",
        "HR": "crna žuna",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "drymar",
        "feeder": ["platforma sa spremnikom", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Tero Linjama"
    },
    milcal: {
        "latin": "Miliaria calandra",
        "EN": "Corn Bunting",
        "HR": "velika strnadica",
        "food": ["sjemenke suncokreta", "usitnjeni kukuruz", "zob", "proso", "orašasti plodovi"],
        "img": "milcal",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"],
        "soundAuthor": "Tero Linjama"
    },
    emcia: {
        "latin": "Emberiza cia",
        "EN": "Rock Bunting",
        "HR": "strnadica cikavica",
        "food": ["sjemenke suncokreta", "usitnjeni kukuruz", "zob", "proso", "orašasti plodovi"],
        "img": "emcia",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"],
        "soundAuthor": "Marc Anderson"
    },
    emcirl: {
        "latin": "Emberiza cirlus",
        "EN": "Cirl Bunting",
        "HR": "crnogrla strnadica",
        "food": ["sjemenke suncokreta", "usitnjeni kukuruz", "zob", "proso", "orašasti plodovi"],
        "img": "emcirl",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"]
    },
    emcit: {
        "latin": "Emberiza citrinella",
        "EN": "Yellowhammer",
        "HR": "žuta strnadica",
        "food": ["sjemenke suncokreta", "usitnjeni kukuruz", "zob", "proso", "orašasti plodovi"],
        "img": "emcit",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"],
        "soundAuthor": "Tero Linjama"
    },
    emsch: {
        "latin": "Emberiza schoeniclus",
        "EN": "Reed Bunting",
        "HR": "močvarna strnadica",
        "food": ["sjemenke suncokreta", "usitnjeni kukuruz", "zob", "proso", "orašasti plodovi"],
        "img": "emsch",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"],
        "soundAuthor": "Stein Ø. Nilsen"
    },
    errub: {
        "latin": "Erithacus rubecula",
        "EN": "European Robin",
        "HR": "crvendać",
        "food": ["sjemenke suncokreta", "voće", "orašasti plodovi", "loj"],
        "img": "errub",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"],
        "soundAuthor": "Joost van Bruggen"
    },
    fricoe: {
        "latin": "Fringilla coelebs",
        "EN": "Eurasian Chaffinch",
        "HR": "zeba",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "fricoe",
        "feeder": ["tlo", "platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma"],
        "soundAuthor": "Timo Tschentscher"
    },
    frimon: {
        "latin": "Fringilla montifringilla",
        "EN": "Brambling",
        "HR": "sjeverna zeba",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "frimon",
        "feeder": ["tlo", "platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma"],
        "soundAuthor": "Tero Linjama"
    },
    gargla: {
        "latin": "Garrulus glandarius",
        "EN": "Eurasian Jay",
        "HR": "šojka",
        "food": ["sve"],
        "img": "gargla",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Tero Linjama"
    },
    lanexc: {
        "latin": "Lanius excubitor",
        "EN": "Great Grey Shrike",
        "HR": "veliki svračak",
        "food": ["loj"],
        "img": "lanexc",
        "feeder": ["lojna kugla"],
        "soundAuthor": "Jarek Matusiak"
    },
    lopcri: {
        "latin": "Parus cristatus",
        "EN": "Crested Tit",
        "HR": "kukmasta sjenica",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "lopcri",
        "feeder": ["platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Piotr Szczypinski"
    },
    loxcur: {
        "latin": "Loxia curvirostra",
        "EN": "Red Crossbill",
        "HR": "krstokljun",
        "food": ["sjemenke suncokreta"],
        "img": "loxcur",
        "feeder": ["hanilica od plastične boce"]
    },
    motal: {
        "latin": "Motacilla alba",
        "EN": "White Wagtail",
        "HR": "bijela pastirica",
        "food": ["sjemenke suncokreta", "usitnjeni kukuruz"],
        "img": "motal",
        "feeder": ["tlo"],
        "soundAuthor": "Tero Linjama"
    },
    parmaj: {
        "latin": "Parus major",
        "EN": "Great Tit",
        "HR": "velika sjenica",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "parmaj",
        "feeder": ["platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Joost van Bruggen"
    },
    pasdom: {
        "latin": "Passer domesticus",
        "EN": "House Sparrow",
        "HR": "vrabac",
        "food": ["sjemenke suncokreta", "usitnjeni kukuruz", "proso", "orašasti plodovi"],
        "img": "pasdom",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"],
        "soundAuthor": "Tero Linjama"
    },
    pasmon: {
        "latin": "Passer montanus",
        "EN": "Eurasian Tree Sparrow",
        "HR": "poljski vrabac",
        "food": ["sjemenke suncokreta", "usitnjeni kukuruz", "proso", "orašasti plodovi"],
        "img": "pasmon",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"],
        "soundAuthor": "Tero Linjama"
    },
    parat: {
        "latin": "Parus ater",
        "EN": "Coal Tit",
        "HR": "jelova sjenica",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "parat",
        "feeder": ["platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Piotr Szczypinski"
    },
    piccan: {
        "latin": "Picus canus",
        "EN": "Grey-faced Woodpecker",
        "HR": "siva žuna",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "piccan",
        "feeder": ["platforma sa spremnikom", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Tero Linjama"
    },
    picvir: {
        "latin": "Picus viridis",
        "EN": "Eurasian Green Woodpecker",
        "HR": "zelena žuna",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "picvir",
        "feeder": ["platforma sa spremnikom", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Terje Kolaas"
    },
    parmon: {
        "latin": "Parus montanus",
        "EN": "Willow Tit",
        "HR": "planinska sjenica",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "parmon",
        "feeder": ["platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma", "lojna kugla"],
        "soundAuthor": "Piotr Szczypinski"
    },
    parpal: {
        "latin": "Parus palustris",
        "EN": "Marsh Tit",
        "HR": "crnoglava sjenica",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "parpal",
        "feeder": ["platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma", "lojna kugla"]
    },
    prumon: {
        "latin": "Prunella modularis",
        "EN": "Hedge Accentor",
        "HR": "sivi popić",
        "food": ["sjemenke suncokreta", "orašasti plodovi"],
        "img": "prumon",
        "feeder": ["tlo"]
    },
    pyrpyr: {
        "latin": "Pyrrhula pyrrhula",
        "EN": "Eurasian Bullfinch",
        "HR": "zimovka",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "pyrpyr",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"]
    },
    serser: {
        "latin": "Serinus serinus",
        "EN": "European Serin",
        "HR": "žutarica",
        "food": ["sjemenke suncokreta", "zob"],
        "img": "serser",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"]
    },
    siteur: {
        "latin": "Sitta europaea",
        "EN": "Wood Nuthatch",
        "HR": "brgljez",
        "food": ["sjemenke suncokreta", "orašasti plodovi", "loj"],
        "img": "siteur",
        "feeder": ["platforma sa spremnikom", "hanilica od plastične boce", "otvorena platforma", "lojna kugla"]
    },
    stredec: {
        "latin": "Streptopelia decaocto",
        "EN": "Eurasian Collared-dove",
        "HR": "gugutka",
        "food": ["sjemenke suncokreta", "usitnjeni kukuruz", "zob", "proso", "orašasti plodovi"],
        "img": "stredec",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"]
    },
    trotro: {
        "latin": "Troglodytes troglodytes",
        "EN": "Winter Wren",
        "HR": "palčić",
        "food": ["usitnjeni kukuruz", "proso"],
        "img": "trotro",
        "feeder": ["sve"]
    },
    turili: {
        "latin": "Turdus iliacus",
        "EN": "Redwing",
        "HR": "mali drozd",
        "food": ["sjemenke suncokreta", "voće", "usitnjeni kukuruz", "proso"],
        "img": "turili",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"]
    },
    turmer: {
        "latin": "Turdus merula",
        "EN": "Eurasian Blackbird",
        "HR": "kos",
        "food": ["sjemenke suncokreta", "voće", "usitnjeni kukuruz", "proso"],
        "img": "turmer",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"]
    },
    turpil: {
        "latin": "Turdus pilaris",
        "EN": "Fieldfare",
        "HR": "drozd bravenjak",
        "food": ["voće", "orašasti plodovi"],
        "img": "turpil",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"]
    },
    turvisc: {
        "latin": "Turdus viscivorus",
        "EN": "Mistle Thrush",
        "HR": "drozd imelaš",
        "food": ["voće", "orašasti plodovi"],
        "img": "turvisc",
        "feeder": ["tlo", "platforma sa spremnikom", "otvorena platforma"]
    }
};